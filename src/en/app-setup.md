# Prepare the project

If you have not already done so, [install Guile on your computer](./en/install.html).

Guile does not officially have a project manager. There are community initiatives such as [Guile Hall](https://gitlab.com/a-sassmannshausen/guile-hall), which you will not use here.

To get started, you will need to create a directory to hold the project sources:
```bash
$ mkdir -p ~/Workspace/guile-grocery-list
```
Create a first file `main.scm` which contains the following expression:
```scheme
(display "hello world")
```
If you can run `guile ~/Workspace/guile-grocery-list/main.scm` and get a `hello world`, you're all set!

## This is what it could look like

```
$ mkdir -p ~/Workspace/guile-grocery-list
$ echo '(display "hello world\n")' > ~/Workspace/guile-grocery-list/main.scm
$ guile ~/Workspace/guile-grocery-list/main.scm
;;; note: auto-compilation is enabled, set GUILE_AUTO_COMPILE=0
;;;       or pass the --no-auto-compile argument to disable.
;;; compiling /home/jeko/Workspace/guile-grocery-list/main.scm
;;; compiled /home/jeko/.cache/guile/ccache/3.0-LE-8-4.6/home/jeko/Workspace/guile-grocery-list/main.scm.go
hello world
```
