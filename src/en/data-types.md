# Data types

The language reference lists no less than 23 types of data (and this is not even exhaustive), from the simplest to the most complex.

Each of these types brings its own set of representations and functionalities.

Some of these types are quite common in programming languages (booleans, numbers, strings). Others are more exotic (symbols).

In the following subchapters, I will quickly and simply implement these data types by way of illustration (I will not cover all their features).
