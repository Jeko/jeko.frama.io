# Edit Guile code

The editor is a very personal choice.

In this section, I'll help you configure some of them to get you started quickly in Guile code editing.

* [Emacs](./en/emacs.html)
