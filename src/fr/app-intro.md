# Créer une application

Les chapitres qui vont suivre te guideront dans le développement d’une application simple avec Guile.

À l’instar des premiers chapitres, je te propose d’aborder l’exercice à travers des pratiques et méthodes de développement que j’apprécie beaucoup (Test Driven Development, Clean Code, Clean Architecture).

Dans un premier temps, tu t’assureras que ton installation de Guile fonctionne.

Dans un deuxième temps, tu implémenteras les premiers cas d’usage de l’application avec leur interface en ligne de commande.

Dans un troisième temps, tu apprendras à tirer profit de bibliothèques tierces pour étendre ton programme (proposer une interface graphique et web par exemple). Tu seras aussi initié à la gestion des dépendances et la distribution de ton programme grâce à un gestionnaire de paquet.

> **Prérequis :** ce tutoriel ne remplace pas une introduction à la programmation et s’attend à ce que tu sois familier avec quelques concepts de base. Tu dois être à l’aise avec l’utilisation de la ligne de commande. Si tu sais déjà programmer dans d’autres languages, ce tutoriel peut être un bon premier contact avec Guile.
>
> **Obtenir de l’aide :** si, à tout instant, tu te sens dépassé ou confus par une fonctionnalité utilisée, jette un oeil à [la référence officielle du langage](https://www.gnu.org/software/guile/manual/html_node/index.html). Elle est distribuée avec la plupart des installations de Guile (accessible depuis un terminal : `info guile`).

Alors, quel type d’application vas-tu créer ? Que dirais-tu d’une application de gestion de liste de courses ? Un petit outil auquel nous donnerions en entrées :
* un drapeau pour indiquer si on ajoute (`--add`) ou retire (`--remove`) un élément de la liste ;
* l’item à ajouter à la liste (`tomates`) ou l’index de l’item à retirer (`12`).

Et qui nous retournerait le contenu de la liste en l’absence d’argument :
```
$ grocery-list
1. Quinoa
2. Champignons
3. Riz
4. Lentilles
5. Épinars
```
> **Note :** ce tutoriel est écrit pour Guile 3. Les exemples de code peuvent être utilisés dans toutes versions de Guile 3.
