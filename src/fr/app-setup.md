# Préparer le projet

Si tu ne l’as pas déjà fait, [installe Guile sur ton ordinateur](./fr/install.html).

Guile ne dispose pas, officiellement, d’un gestionnaire de projet. Il existe des initiatives communautaires telles que [Guile Hall](https://gitlab.com/a-sassmannshausen/guile-hall), que tu n’utiliseras pas ici.

Pour démarrer, tu auras besoin de créer un répertoire pour accueillir les sources du projet :
```bash
$ mkdir -p ~/Workspace/guile-grocery-list
```

Crées-y un premier fichier `main.scm` qui contient l’expression suivante :

```scheme
(display "hello world\n")
```

Si tu peux exécuter `guile ~/Workspace/guile-grocery-list/main.scm` et obtenir un `hello world`, c’est tout bon !

## Voilà à quoi cela pourrait ressembler

```
$ mkdir -p ~/Workspace/guile-grocery-list
$ echo '(display "hello world\n")' > ~/Workspace/guile-grocery-list/main.scm
$ guile ~/Workspace/guile-grocery-list/main.scm
;;; note: auto-compilation is enabled, set GUILE_AUTO_COMPILE=0
;;;       or pass the --no-auto-compile argument to disable.
;;; compiling /home/jeko/Workspace/guile-grocery-list/main.scm
;;; compiled /home/jeko/.cache/guile/ccache/3.0-LE-8-4.6/home/jeko/Workspace/guile-grocery-list/main.scm.go
hello world
```
