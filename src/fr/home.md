![](https://www.gnu.org/software/guile/static/base/img/kid-programming-a-robot.png)

# Contexte

Apprendre un nouveau langage de programmation est une entreprise fastidieuse. En ce qui me concerne, j’ai trouvé le début de mon apprentissage du langage Guile laborieux. Pour plusieurs raisons :

* J’ai démarré sans expérience en Lisp ou ses dialectes (comme Scheme).
* Je ne connaissais personne d’expérimenté (ou même désireux d’apprendre) en Lisp ou ses dialectes.
* Je ne suis pas un chercheur.
* Les mathématiques ne sont pas ma vocation.
* J’aime bidouiller pour apprendre, suivre un tutoriel pour développer un petit logiciel simple, lire des articles de blog, regarder une vidéo de live-coding… 

Bref, à première vue, Guile et moi, ce n’était pas fait pour coller.

Dans le **Carnet du hacker Guile**, je veux rassembler tout ce dont un apprenti hacker aurait, selon moi, besoin pour passer du stade de débutant (qui découvre la syntaxe) à celui d’intermédiaire (qui crée des programmes simples). 

> C’est la ressource que j’aurais voulu trouver quand j’ai démarré.

## Un complément à la littérature du langage Scheme

Lisp est une des plus vielles familles de langage de programmation.

Scheme est un dialecte du langage de programmation Lisp (oui, il porte le nom de sa famille). La spécification du langage Scheme évolue au rythme de révisions. Plusieurs implémentations du langage Scheme existent (telle que Guile, Chez Scheme, CHICKEN, Gambit). Chaque implémentation respectant tout ou partie d’une ou plusieurs versions de la spécification.

Ainsi, les ressources pour apprendre Guile se mêlent avec celles pour apprendre Scheme, voire Lisp. Mais elles divergent en en termes de fonctionnalités/api/modules.

Cela rend l’expérience d’apprentissage inconfortable lorsqu’on trouve une ressource illustrant un concept de Scheme avec quelques lignes de code que le REPL de Guile ne peut pas interpréter.

Le **Carnet du hacker Guile** est une ressource dédiée spécifiquement à Guile. Tous les fonctions/modules utilisés dans cet ouvrage font partie de la distribution de Guile.

## Un complément à la référence du langage Guile

Comme son nom l’indique, une référence est un ouvrage sur lequel on revient régulièrement avec un besoin bien précis. Elle fournit la liste exhaustive des fonctionnalités du langage sans pour autant expliquer comment les mettre en oeuvre (ça arrive mais ce n’est pas systématique).

Le **Carnet du hacker Guile** illustre certains de ces concepts par des mises en situations structurées et concrètes.

## Une autre approche

Mon style de programmation est très influencé par un certain nombre de principes et pratiques (Test Driven Development, Clean Code, Clean Architecture…) que j’ai découvert à travers des langages orientés objet et statiquement typés, dans un environnement plus industriel qu’académique.

Ces approches ont été formalisées dans des livres que je recommande :
* « Test Driven Development by Example », de Kent Beck
* « Clean Code : A Handbook of Agile Software Craftsmanship » de Robert C. Martin
* « Clean Architecture : A Craftsman's Guide to Software Structure and Design », de Robert C. Martin

D’ailleurs, c’est en cherchant des ressources sur le *TDD* que je suis un jour tombé [quii.gitbook.io](http://quii.gitbook.io) qui a été la première source d’inspiration de ce livre.

Le **Carnet du hacker Guile** explore ces concepts à travers Guile, un langage fonctionnel au typage dynamique.

## De quoi tu auras besoin

* Un ordinateur et le système GNU/Linux de ton choix.
* Un peu d’expérience en programmation.
* L’envie d’apprendre.

### Ressources pratiques à propos de Guile/Scheme

Si tu en ressens le besoin, voici quelques ressources pour t’échauffer avant de lire le **Carnet du hacker Guile**.
* [The Scheme Primer](https://spritely.institute/static/papers/scheme-primer.html)
* [Learn Scheme in 15 minutes](https://artanis.dev/scheme.html)

## Retours

S’il te plait, aide-moi à améliorer cet ouvrage ! Fais-moi part de toutes tes remarques au fur et à mesure que tu lis :
* est-ce que j’ai sauté une étape ?
* des incohérences ?
* besoin d’information supplémentaires ?
* quelque chose ne fonctionne pas ?
* des fautes d’orthographe ?
* pas assez d’amour ?
* tu kiff à donf ?

Envoie-moi tout ça :
* [email](mailto:jeremy@korwin-zmijowski.fr)
* [twitter](https://twitter.com/JeremyKorwin)
* [linkedin](www.linkedin.com/in/jeko)
* [mastodon](@jeko@framapiaf.org)
* [instagram](https://www.instagram.com/jeremy_kz/)
* en ouvrant un ticket sur le [dépôt](https://framagit.org/Jeko/jeko.frama.io)
