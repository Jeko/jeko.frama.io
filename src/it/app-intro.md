# Creare un'applicazione

I capitoli seguenti ti guideranno nello sviluppo di una semplice applicazione con Guile.

Come nei primi capitoli, propongo di affrontare l'esercizio attraverso pratiche e metodi di sviluppo che apprezzo molto (Test Driven Development, Clean Code, Clean Architecture).

Cercherò di darti le conoscenze essenziali per andare avanti e dove trovare ulteriori informazioni (se vuoi andare oltre).

Per prima cosa assicurati che la tua installazione di Guile funzioni.

Secondariamente, implementa i primi casi d'uso dell'applicazione con l'interfaccia a riga di comando. Esporrò alcuni concetti chiave. Questa sarà anche l'occasione per impostare il tracciamento delle versioni e la documentazione.

Infine, imparerai a sfruttare librerie di terze parti per estendere il tuo programma (ad esempio, per avere un'interfaccia grafica e web). Imparerai anche a gestire le dipendenze e a distribuire il tuo programma utilizzando un gestore di pacchetti.

> **Prerequisiti:** Questa esercitazione non sostituisce un'introduzione alla programmazione e si aspetta che l'utente abbia familiarità con alcuni concetti di base. Dovresti essere a tuo agio nell'uso della riga di comando. Se sai già programmare in altri linguaggi, questo tutorial può essere un buon primo contatto con Guile.
>
> **Ottenere aiuto:** Se in qualsiasi momento ti senti sopraffatto o confuso da una funzione che stai utilizzando, dai un'occhiata a [la reference ufficiale del linguaggio] (https://www.gnu.org/software/guile/manual/html_node/index.html). Viene distribuita con la maggior parte delle installazioni di Guile (accessibile da terminale: `info guile`).

Quindi, che tipo di applicazione creerai? Che ne dici di un'applicazione per la lista della spesa? Una piccola utility a cui daremo come input :
* un flag che indica cosa aggiungere (`--add`) o rimuovere (`--remove`) dalla lista;
* l'elemento che vogliamo aggiungere (`tomatoes`) o l'indice di quello che vogliamo rimuovere (`12`).

e che restituisce il contenuto dell'elenco in assenza di argomenti:
```
$ grocery-list
1. Quinoa
2. Mushrooms
3. Rice
4. Lentils
5. Spinach
```
> Nota: Questo tutorial è stato scritto per Guile 3. Gli esempi di codice possono essere utilizzati in tutte le versioni di Guile 3.
