# Prepara il progetto

Se non l'hai ancora fatto, [installa Guile sul computer](./it/install.html).

Guile non ha ufficialmente un project manager. Esistono iniziative comunitarie come [Guile Hall](https://gitlab.com/a-sassmannshausen/guile-hall), che non verranno utilizzate in questa sede.

Per iniziare, è necessario creare una cartella che contenga i sorgenti del progetto:
```bash
$ mkdir -p ~/Workspace/guile-grocery-list
```
Crea il file `main.scm` contenente la seguente espressione:
```scheme
(display "hello world")
```
Se eseguendo `guile ~/Workspace/guile-grocery-list/main.scm` ottieni un hello `hello world`, sei pronto!

## Questo è l'aspetto che potrebbe avere

```
$ mkdir -p ~/Workspace/guile-grocery-list
$ echo '(display "hello world\n")' > ~/Workspace/guile-grocery-list/main.scm
$ guile ~/Workspace/guile-grocery-list/main.scm
;;; note: auto-compilation is enabled, set GUILE_AUTO_COMPILE=0
;;;       or pass the --no-auto-compile argument to disable.
;;; compiling /home/jeko/Workspace/guile-grocery-list/main.scm
;;; compiled /home/jeko/.cache/guile/ccache/3.0-LE-8-4.6/home/jeko/Workspace/guile-grocery-list/main.scm.go
hello world
```
