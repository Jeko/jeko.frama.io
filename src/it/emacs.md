# Emacs

[Emacs](https://www.gnu.org/software/emacs/) è l'editor che fornisce il miglior support per Guile.

## Installazione

Puoi installarlo nello stesso modo in cui hai installato Guile (qui sotto l'esempio con Guix) :

```bash
$ guix install emacs
```

## Configurazione

Questo libro non ha lo scopo di insegnare come usare Emacs. Quindi ti propongo una configurazione pronta all'uso per rendere più piacevole la scrittura di programmi Guile con Emacs. Sentiti libero di prendere solo ciò che ti interessa!

### Supporto visuale

#### Evidenziare la linea del cursore

```lisp
;; paste this lines into your emacs config file
(global-hl-line-mode +1)
```

Disattivato

![Without global-hl-line-mode](images/without-current-line-highlight.gif)

Attivato

![With global-hl-line-mode](images/with-current-line-highlight.gif)

#### Evidenziare la coppia di delimitatori sotto il cursore

```lisp
;; paste this lines into your emacs config file
(show-paren-mode 1)
(setq show-paren-delay 0)
```

Disattivato

![Without show-paren-mode](images/without-show-parens.gif)

Attivato

![With show-paren-mode](images/with-show-parens.gif)

### Supporto Operativo

#### " rendere l'hacking di Scheme in Emacs (ancora più) divertente "

Se si potesse installare una sola estensione, sarebbe questa.

*Installazione*

```bash
$ guix install emacs-geiser emacs-geiser-guile
```

*Attivazione*

```
M-x run-guile
```

*Utilizzo*

Puoi fare riferimento a [https://www.nongnu.org/geiser/Cheat-sheet.html#Cheat-sheet](https://www.nongnu.org/geiser/Cheat-sheet.html#Cheat-sheet)

#### Aggiungere l'auto completamento Geiser :

*Installazione*

```bash
$ guix install emacs-ac-geiser
```

*Attivazione*

```lisp
;; paste this lines into your emacs config file
(ac-config-default)
(require 'ac-geiser)
(add-hook 'geiser-mode-hook 'ac-geiser-setup)
(add-hook 'geiser-repl-mode-hook 'ac-geiser-setup)
(eval-after-load "auto-complete"
  (add-to-list 'ac-modes' geiser-repl-mode))
```

Disattivato

![Without ac-geiser](images/without-ac-geiser.gif)

Attivato

![With ac-geiser](images/with-ac-geiser.gif)

#### Modificare il codice in base alla struttura dell'espressione S (S-expression)

*Installazione*

```bash
$ guix install emacs-paredit
```

*Attivazione*

```lisp
;; paste this lines into your emacs config file
(require 'paredit)
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'scheme-mode-hook #'enable-paredit-mode)
```

*Utilizzo*

Non potrei fare di meglio di [questa guida scritta da Dan Midwood](http://danmidwood.com/content/2014/11/21/animated-paredit.html).

### Supporto alla rifattorizzazione

#### Modificare più stringhe di caratteri simultaneamente

*Installazione*

```bash
$ guix install emacs-iedit
```
*Attivazione*

```lisp
;; paste this lines into your emacs config file
(require 'iedit)
```

*Utilizzo*

`C-;` su una parola per modificare tutte le sue occorrenze.
`C-0 C-;` su una parola per modificare tutte le sue occorrenze nella regione attiva.

#### Modificare contemporaneamente e nello stesso modo più punti del file.

 *Installazione*

```bash
$ guix install emacs-multiple-cursors
```

*Attivazione*

```lisp
;; paste this lines into your emacs config file
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
```

*Utilizzo*

`C->` Aggiungere un cursore alla riga successiva
`C-<` Aggiungere un cursore alla riga precedente

Al termine della modifica, `C-g`.

#### Estrarre variabili e funzioni


*Installazione*

```lisp
;; paste this lines into your emacs config file
(if (not (package-installed-p 'emr))
    (progn
      (package-refresh-contents)
      (package-install 'emr))))
```

*Attivazione*

```lisp
;; paste this lines into your emacs config file
(require 'emr)
(autoload 'emr-show-refactor-menu "emr")
(define-key prog-mode-map (kbd "M-RET") 'emr-show-refactor-menu)
(eval-after-load "emr" '(emr-initialize))
(global-set-key (kbd "M-v") 'emr-scm-extract-variable)
(global-set-key (kbd "M-f") 'emr-scm-extract-function)
```

*Utilizzo*

`M-v` Estrarre un variabile
`M-f` Estrarre una funzione
