# Sistemalo!

Leggere e comprendere gli avvertimenti e i messaggi di errore dell'interprete è molto utile. Per questo ti invito a una serie di mini-esercizi in cui ti sfido a sistemare codice non funzionante in base a ciò che l'interprete ti restituisce.

## Esercizio n°1

```scheme
(define congratulations "Well done !")
(display congralutations)
```

La valutazione di queste espressioni restituisce:
```
;;; <unknown-location>: warning: possibly unbound variable `congralutations'
ice-9/boot-9.scm:1685:16: In procedure raise-exception:
Unbound variable: congralutations

Entering a new prompt.  Type `,bt' for a backtrace or `,q' to continue.
```
