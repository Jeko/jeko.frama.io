![](https://www.gnu.org/software/guile/static/base/img/kid-programming-a-robot.png)

# Contesto

Imparare un nuovo linguaggio di programmazione è un'impresa noiosa. Nel mio caso, ho trovato l'inizio del mio apprendimento del linguaggio Guile laborioso.

* Non avevo nessuna esperienza in Lisp o nei suoi dialetti (come Scheme).
* Non conoscevo nessuno con esperienza (o disposto a imparare) il Lisp o i suoi dialetti.
* Non sono un ricercatore.
* La matematica non è la mia vocazione.
* Mi piace smanettare per imparare, seguire tutorial per sviluppare semplici software, leggere articoli sui blog, guardare video in diretta...

In breve, di primo acchito, io e Guile, non avevamo molto in comune.

In **Guile Hacker's Notebook**, voglio raccogliere tutto quello che penso possa essere utile a un apprendista hacker per passare da principiante (scoprire la sintassi) a intermedio (creare programmi semplici).

> Questa è la risorsa che avrei voluto trovare quando ho iniziato.

## Un complemento alla letteratura sul linguaggio Scheme

Lisp è una delle più antiche famiglie di linguaggi di programmazione.

Scheme è un dialetto del linguaggio di programmazione Lisp (sì, prende il nome dalla sua famiglia). La specifica del linguaggio Scheme si evolve al ritmo delle revisioni. Esistono diverse implementazioni del linguaggio Scheme (come Guile, Chez Scheme, CHICKEN, Gambit). Ogni implementazione rispetta tutte o parte di una o più versioni della specifica.

Così, le risorse per imparare Guile sono mescolate con quelle per imparare Scheme o anche Lisp. Ma divergono in termini di caratteristiche/api/moduli.

Questo rende l'esperienza di apprendimento difficoltosa quando si segue una risorsa che illustra un concetto di Scheme con alcune linee di codice che REPL di Guile non può interpretare.

**Guile Hacker's Notebook** è una risorsa dedicata specificamente a Guile. Tutte le funzioni/moduli utilizzati in questo libro fanno parte della distribuzione Guile.

## Un complemento alla reference sul linguaggio Guile.

Come indica il nome, una reference è un libro a cui si torna regolarmente per una necessità specifica. Esso fornisce l'elenco esaustivo delle caratteristiche del linguaggio senza spiegare come implementarle (succede, ma non è sistematico).

**Guile Hacker's Notebook** illustra questi concetti attraverso situazioni strutturate e concrete.

## Un approccio diverso

Il mio stile di programmazione è molto influenzato da una serie di principi e pratiche (sviluppo guidato con test, codice pulito, architettura pulita...) che ho scoperto attraverso linguaggi orientati agli oggetti e tipizzati staticamente, in un ambiente più industriale che accademico.

Questi approcci sono stati formalizzati nei seguenti libri che raccomando:
* "Test Driven Development by Example", by Kent Beck
* "Clean Code: A Handbook of Agile Software Craftsmanship" by Robert C. Martin
* "Clean Architecture: A Craftsman's Guide to Software Structure and Design", by Robert C. Martin

A proposito, è stato mentre cercavo risorse su *TDD* che mi sono imbattuto in [quii.gitbook.io](http://quii.gitbook.io) che è stato la prima fonte di ispirazione per questo libro.

**Guile Hacker's Notebook** esplora questi concetti attraverso Guile, un linguaggio funzionale con tipizzazione dinamica.

## Cosa ti serve

- Un computer e un sistema GNU/Linux a tua scelta.
- Un po' di esperienza di programmazione.
- Il desiderio di imparare.

### Risorse pratiche su Guile/Scheme

Se ne sentite il bisogno, ci sono delle risorse per "scaldarvi" prima di leggere **Guile Hacker's Notebook**.
* [The Scheme Primer](https://spritely.institute/static/papers/scheme-primer.html)
* [Learn Scheme in 15 minutes](https://artanis.dev/scheme.html)

## Feedbacks

Per favore, aiutatemi a migliorare questo libro! Inivatemi i vostri commenti:
- ho saltato qualche passaggio?
- qualche incongruenza?
- bisogno di maggiori informazioni?
- qualcosa non funziona?
- errori di battitura?
- non c'è abbastanza amore?
- Ti piace?

Mandate tutto a:
- [email](mailto:jeremy@korwin-zmijowski.fr)
- [twitter](https://twitter.com/JeremyKorwin)
- [linkedin](www.linkedin.com/in/jeko)
- [mastodon](@jeko@framapiaf.org)
- [instagram](https://www.instagram.com/jeremy_kz/)
- aprite un ticket sul [repository](https://framagit.org/Jeko/jeko.frama.io)
