# Installare Guile

## Installazione

### su GNU/Linux

Installare Guile è facilissimo utilizzando un package manager.

**Usando Guix (raccomandato)**

```bash
$ guix install guile
```

**Usando apt (su Debian e derivate)**

```bash
# apt install guile-3.0
```

**Usando Yum (su RedHat e derivate)**

```bash
# yum install guile
```

**Usando Pacman (su ArchLinux e derivate)**

```bash
# pacman -S guile
```

### su MacOS

La procedura di installazione su MacOS è disponibile [qui](https://github.com/aconchillo/homebrew-guile).

### su Windows

Puoi installare il sottosistema Ubuntu per Windows (da Microsoft Store), seguire le istruzioni per l'installazione usando apt, scaricare Guile e iniziare a smanettare!

## Configurazione

Guile include uno strumento chiamato REPL (Read-Eval-Print-Loop), una sorta di super interprete. Senza configurazione però, risulta molto rudimentale e non molto pratico.

![repldown](images/guile-brut.gif)

Niente di molto complicato ma è necessario attivare due moduli (uno dei quali è necessario installarlo manualmente).

```bash
$ guix package -i guile-colorized
```

Se non usi Guix, puoi trovare le istruzioni di installazione per questo modulo [su questa pagina](https://gitlab.com/NalaGinrut/guile-colorized).

Una volta installato, puoi configurare il tuo interprete modificando il file `~/.guile` (creane uno nuovo se non esiste) aggiungendo queste righe:

```scheme
(use-modules (ice-9 readline)
             (ice-9 colorized))

(activate-readline)
(activate-colorized)
```

Con questo REPL è ora più colorato, ha una cronologia in cui è possibile navigare e un aiuto visivo per chiudere le parentesi.

![replup](images/guile-net.gif)
